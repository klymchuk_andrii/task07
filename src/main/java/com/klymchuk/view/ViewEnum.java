package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ViewEnum{
    Logger logger;
    private Controller controller;
    private ArrayList<String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public enum MenuItem{

        PRINT(1), PUT(2), GET(3), REMOVE(4), EXIT(5);
        int number;
        MenuItem(int i) {
            number = i;
        }

        public int getNumber() {
            return number;
        }
    }

    public ViewEnum() {
        input = new Scanner(System.in);

        logger = LogManager.getLogger(ViewEnum.class);
        controller = new ControllerImplementation();


        menu = new ArrayList<>();
        menu.add("  1 - print tree");
        menu.add( "  2 - put element");
        menu.add( "  3 - get element");
        menu.add("  4 - remove element");
        menu.add( "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printMap);
        methodsMenu.put("2", this::put);
        methodsMenu.put("3", this::get);
        methodsMenu.put("4", this::remove);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (MenuItem str : MenuItem.values()) {
            logger.trace(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (MenuItem.valueOf(keyMenu)){
                    case PRINT:
                        printMap();
                        break;
                    case GET:
                        get();
                        break;
                    case PUT:
                        put();
                        break;
                    case REMOVE:
                        remove();
                        break;
                    case EXIT:
                        return;
                    default:
                        logger.warn("Wrong input!");
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void printMap() {
        logger.trace(controller.print());
    }


    private void put() {
        logger.trace("Put string key and value to add: \n");
        logger.trace(controller.put(input.nextLine(), input.nextLine()));
    }

    private void get() {
        logger.trace("Put string key to find: \n");
        logger.trace(controller.get(input.nextLine()));
    }

    private void remove() {
        logger.trace("Put string key to remove: \n");
        logger.trace(controller.remove(input.nextLine()));
    }
}
