package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;


public class ViewMap implements Printable{
    Logger logger;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public ViewMap() {
        input = new Scanner(System.in);

        logger = LogManager.getLogger(ViewMap.class);
        controller = new ControllerImplementation();

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print treeMap");
        menu.put("2", "  2 - put element");
        menu.put("3", "  3 - get element");
        menu.put("4", "  4 - remove element");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::print);
        methodsMenu.put("2", this::put);
        methodsMenu.put("3", this::get);
        methodsMenu.put("4", this::remove);
    }

    private void Menu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            Menu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    public void print() {
        logger.info(controller.print());
    }


    private void put() {
        logger.info("Put string key and value to add: \n");
        logger.debug(controller.put(input.nextLine(), input.nextLine()));
    }

    private void get() {
        logger.info("Put string key to find: \n");
        logger.info(controller.get(input.nextLine()));
    }

    private void remove() {
        logger.info("Put string key to remove: \n");
        logger.info("remove this value: "+controller.remove(input.nextLine()));
    }
}
