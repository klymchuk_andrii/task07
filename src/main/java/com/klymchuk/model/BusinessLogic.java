package com.klymchuk.model;

public class BusinessLogic implements Model{
    BinaryTreeMap<String, String> treeMap;

    public BusinessLogic() {
        this.treeMap = new BinaryTreeMap<>();
    }

    @Override
    public int size() {
        return treeMap.size();
    }

    @Override
    public boolean isEmpty() {
        return treeMap.isEmpty();
    }

    @Override
    public String get(Object key) {
        return treeMap.get(key);
    }

    @Override
    public String put(String key, String value) {
        return treeMap.put(key,value);
    }

    @Override
    public String remove(Object key) {
        return treeMap.remove(key);
    }

    @Override
    public String print() {
        String rezult = "";
        for (BinaryTreeMap.Entry<String, String> entry : treeMap.entrySet()) {
            rezult += ("key: " + entry.getKey() + " value: " + entry.getValue());
        }
        return rezult;
    }
}