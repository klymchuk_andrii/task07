package com.klymchuk.controller;

import com.klymchuk.model.BusinessLogic;
import com.klymchuk.model.Model;

public class ControllerImplementation implements Controller{
    private Model model;

    public ControllerImplementation(){
        model = new BusinessLogic();
    }

    @Override
    public int size() {
        return model.size();
    }

    @Override
    public boolean isEmpty() {
        return model.isEmpty();
    }

    @Override
    public String get(Object key) {
        return model.get(key);
    }

    @Override
    public String put(String key, String value) {
        return model.put(key,value);
    }

    @Override
    public String remove(Object key) {
        return model.remove(key);
    }

    @Override
    public String print() {
        return model.print();
    }
}
